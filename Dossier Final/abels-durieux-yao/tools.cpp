#include "tools.h"
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "task.h"
using namespace std;
Tools::Tools()
{

}

vector<Task>Tools::parseFile(ifstream& fileStream) {

	vector<Task> allTasks;
	int offset, period, deadline, wcet, memory;
	while (fileStream >> offset >> period >> deadline >> wcet >> memory)
	{
		allTasks.push_back(Task(offset, period, deadline, wcet, memory));
	}
	return allTasks;
}


int Tools::lcm(int a, int b) {

	int tmp_a = a;
	int tmp_b = b;
	int gcd;

	//Compute greatest common divisor
	while (tmp_b != 0) {
		gcd = tmp_b;
		tmp_b = tmp_a % tmp_b;
		tmp_a = gcd;
	}

	return a / gcd * b;
}


int Tools::lcmOfTaskSet(std::vector<Task> taskSet) {
	int lcmVal = 1;
	for (Task t : taskSet)
	{
		lcmVal = lcm(lcmVal, t.period);
	}
	return lcmVal;
}


int Tools::maxOffset(std::vector<Task> taskSet) {
	int maxVal = -1;
	for (Task t : taskSet)
	{
		maxVal = max(t.offset, maxVal);
	}
	return maxVal;
}

double Tools::getTaskSetUtilization(std::vector<Task> taskSet)
{
	double utilization = 0;
	for (Task t : taskSet)
	{
		utilization += t.utilization;
	}
	return utilization;
}

void Tools::exportTasksToFile(string file, vector<Task> taskSet) {
	ofstream fichier(file, ios::trunc);  //Open file outputstream

	if (fichier.is_open())
	{
		for (Task t : taskSet) {
			fichier << t.toString() << endl;
		}
		fichier.close();
	}
	else
		cerr << "Couldn't write to outputfile '" << file << "'" << endl;
}



