#ifndef GENERATOR_H
#define GENERATOR_H
#include <vector>
#include "task.h"
class Generator
{
public:
    Generator();
    std::vector<Task> generateTasks(double utilization,int nbTasks,int memory, bool forceMaxRam);
    std::vector<int> generateValues(int low, int high,int nbTasks,int factor);
};

#endif // GENERATOR_H
