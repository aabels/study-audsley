#include <iostream>
#include <cstdlib>
#include "generator.h"
#include "tools.h"

using namespace std;

int main(int argc, char * argv[])
{
    if(argc>8){
    Generator gen;
    double utilization=0.0;
    int memory=0;
    int nbTasks=0;
    string file;
    int i = 1;
    while (i + 1 < argc)
    {
        if(std::string(argv[i])=="-u"){
               utilization = atoi(argv[i + 1]);
        }else if(std::string(argv[i])=="-m"){
              memory = atoi(argv[i + 1]);
        }else if(std::string(argv[i])=="-n"){
                nbTasks = atoi(argv[i + 1]);
        }else if(std::string(argv[i])=="-o"){
               file = argv[i + 1];
        }else
        {
            cerr << "Invalid arguments provided, syntax:" << endl;
            cerr << argv[0] << " -u 70 -m 3 -n 8 -o tasks.txt" << endl;
            return EXIT_FAILURE;
        }

        i+=2;
     }

    vector<Task> tasks=gen.generateTasks(utilization, nbTasks, memory,false) ;
    Tools::exportTasksToFile( file,tasks);


    }
    else {
        cerr << "Missing arguments, syntax:" << endl;
        cerr << argv[0] << " -u <utilization> -m <max_task_memory> -n <number_of_tasks> -o <outputfile>" << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

