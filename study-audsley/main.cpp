#include <iostream>
#include "generator.h"
#include "ram.h"
#include "tools.h"
#include "scheduler.h"
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <string.h>
#include <fstream>
#include <map>

#include <time.h>
using namespace std;

std::vector<std::string> split(const std::string &s, char delim);

int main(int argc, char* argv[])
{

	if (argc < 2) 
	{
		cerr << "Missing arguments, syntax:" << endl;
		cerr << argv[0] << " <simulation_count> [-f] [-u <utilization_range>] [-n <task_range>] [-r <total_memory_range>] [-p <taskram_range>] [-o <outputfile>] [-l <loadTime>]." << endl;
		cerr << "Range format: lower_bound:step:upper_bound (From 10 to 100 by steps of 10; 10:10:90)." << endl;
		
	}
	else {
		int loadTime = 1;

		int simulations = atoi(argv[1]);
		bool fullSimulation = false;

		//Default ranges
		int utilisation_upper_bound = 65;
		int utilisation_lower_bound = 65;
		int utilisation_step = 1;

		int tasks_upper_bound = 4;
		int tasks_lower_bound = 4;
		int tasks_step = 1;

		int memory_upper_bound = 40;
		int memory_lower_bound = 40;
		int memory_step = 1;

		int taskram_upper_bound = 4;
		int taskram_lower_bound = 4;
		int taskram_step = 1;

		string filename = "output-";
		filename += to_string(simulations) + "-";
		bool useArgFileName = false;
		string argFileName;
		int i = 2;
		while (i< argc)
		{
			if (strcmp(argv[i] ,"-f")==0)
			{
				filename += "full-";
				fullSimulation = true;
				i++;
			}
			else if(i+1 < argc)
			{
				string argValue = argv[i + 1];
				if (strcmp(argv[i], "-o") == 0) //Outputfile
				{
					useArgFileName = true;
					argFileName = argValue;
				}
				if (strcmp(argv[i], "-l") == 0) //Load time
				{
					loadTime = atoi(argv[i+1]);
				}
				else {
					vector<string> argValues = split(argv[i + 1], ':');

					if (argValues.size() < 3) //If a single value is provided, set the range to this value. 30 -> 30:1:30
					{
						argValues.push_back("1");
						argValues.push_back(argValues[0]);
					}
					
					if (strcmp(argv[i], "-u") == 0)
					{
						filename += "u" + argValue;
						utilisation_lower_bound = atoi(argValues[0].c_str());
						utilisation_step = atoi(argValues[1].c_str());
						utilisation_upper_bound = atoi(argValues[2].c_str());
					}
					else if (strcmp(argv[i], "-n") == 0)
					{

						filename += "n" + argValue;
						tasks_lower_bound = atoi(argValues[0].c_str());
						tasks_step = atoi(argValues[1].c_str());
						tasks_upper_bound = atoi(argValues[2].c_str());

					}
					else if (strcmp(argv[i], "-r") == 0)
					{

						filename += "r" + argValue;
						memory_lower_bound = atoi(argValues[0].c_str());
						memory_step = atoi(argValues[1].c_str());
						memory_upper_bound = atoi(argValues[2].c_str());
					}
					else if (strcmp(argv[i], "-p") == 0)
					{

						filename += "p" + argValue;
						taskram_lower_bound = atoi(argValues[0].c_str());
						taskram_step = atoi(argValues[1].c_str());
						taskram_upper_bound = atoi(argValues[2].c_str());
					}
					
				}
				i += 2;
			}

		}

		
		if (useArgFileName)
			filename = argFileName;
		else
		{
			filename += ".csv";
			std::replace(filename.begin(), filename.end(), ':', '~');
		}

		std::cout << "Saving simulations to "<< filename << " ..." << endl;
		ofstream output(filename, ios::binary);
		if (output.is_open()) {
			output << "Tasksets simulated:" << simulations << endl;
			output << "Utilisation;setSize;systemload;rampages;memory" << endl;

			Generator gen;

			cout << "Pending simulation:" << endl;
			cout << "Taskram\t\tTotalMemory\tTaskCount\tUtilization" << endl;

			//Loop through all ranges
			for (int u = utilisation_lower_bound; u <= utilisation_upper_bound; u += utilisation_step)
			{

				for (int t = tasks_lower_bound; t <= tasks_upper_bound; t += tasks_step)
				{

					for (int m = memory_lower_bound; m <= memory_upper_bound; m += memory_step)
					{

						for (int r = taskram_lower_bound; r <= taskram_upper_bound; r += taskram_step)
						{

							cout << r << "\t\t" << m << "\t\t" << t << "\t\t" << u << endl;

							for (int s = 0; s < simulations;)
							{

								double utilization = rand() % utilisation_step + u;;
								int nbTasks = rand() % tasks_step + t;
								int maxTaskRam = rand() % taskram_step + r;

								int totalRam = maxTaskRam * (rand() % memory_step + m); //Totalram proportional to the maxTaskRam

								vector<Task> taskSet = gen.generateTasks(utilization, nbTasks, maxTaskRam, true);


								if (!taskSet.empty()) {
									double utilization = Tools::getTaskSetUtilization(taskSet);
									if (utilization < 1) {

										Ram ram(totalRam, loadTime);

										int period = Tools::lcmOfTaskSet(taskSet);
										int maxOffset = Tools::maxOffset(taskSet);

										Scheduler sched(ram, period, maxOffset);
										if (sched.audsley(taskSet)) {
											if (sched.isSchedulable(sched.finalSet, true)) {
												output << Tools::getTaskSetUtilization(sched.finalSet) << ";" << taskSet.size() << ";" << sched.getSystemLoad() << ";" << totalRam << ";" << maxTaskRam << endl;

												if (fullSimulation) //If a full simulation is required we only count successfull systems
													s++;
											}
										}
									}
								}

								if (!fullSimulation)
									s++;

							}
						}
					}
				}
			}
			output.close();
		}
		else
		{
			cerr << "Can't save to " << filename << endl;
		}
	}
    return 0;
}


std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;

	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}
