#include "generator.h"
#include <random>
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <cmath>
#include "task.h"
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

Generator::Generator()
{
	srand(time(NULL));
}

std::vector<Task> Generator::generateTasks(double utilization, int nbTasks, int maxTaskMemory,bool forceMaxRam) {

	vector<Task> taskSet;

	vector<int> taskPeriods = generateValues(5, 10, nbTasks, 10); //Generate offsets and periods which are multiples of 10 to avoid huge periods when simulating audsley.
	vector<int> taskOffsets = generateValues(0, 10, nbTasks, 10);
	int minTaskMemory = forceMaxRam? maxTaskMemory : 1;
	
	vector<int> taskMemories = generateValues(minTaskMemory, maxTaskMemory, nbTasks, 1);

	while (nbTasks>0) {
		int taskIndex = nbTasks - 1;

		double task_utilization;

		if (utilization <= nbTasks)			//Make sure every task has at least 1% utilization
			task_utilization = 1;
		else if (nbTasks == 1)				//The last tasks uses up all the remaining utilization
			task_utilization = utilization;
		else								//Assign random utilization		
			task_utilization = rand() % (int)ceil(utilization - nbTasks);

		int wcet = round((taskPeriods.at(taskIndex)*(task_utilization)) / 100); 
		wcet = max(1, wcet);
		
		//Use (rounded) wcet value to compute this tasks effective utilization
		task_utilization = double(100 * wcet) / taskPeriods.at(taskIndex);
		utilization -= task_utilization;

		int deadline = rand() % max((taskPeriods.at(taskIndex) - wcet), 1) + wcet; //Deadline between wcet and period -> max avoids division by zero

		taskSet.push_back( Task(taskOffsets.at(taskIndex), 
								taskPeriods.at(taskIndex),
								deadline,
								wcet, 
								taskMemories.at(taskIndex)));

		nbTasks--;
	}
	return taskSet;
}




vector<int> Generator::generateValues(int low, int high, int nbTasks, int factor) {

	vector<int> periods(nbTasks, -1);
	for (size_t i = 0; i<periods.size(); i++) {
		periods.at(i) = factor*(rand() % (high - low + 1) + low);
	}
	return periods;
}
